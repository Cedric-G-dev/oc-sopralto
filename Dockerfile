FROM python:slim

LABEL Cedric GUIMIER

COPY . /app

WORKDIR /app

RUN apt-get update -y && \
    apt-get install -y python3 && \
    apt-get install -y python3-pip && \
    pip3 install pytest && \
    pip3 install -r requirements.txt

EXPOSE 5000 

CMD ["python3","application.py"]

